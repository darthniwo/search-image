# README

This README document whatever steps are necessary to get the application up and running.

### What is this repository for?

- In this repository resides the exercise for Image Search app
- Version: 0.1.0

### Initial setup & running the app

- On the root folder run `npm run init` to initialize install commands
- For starting run `npm run dev`
- API will be running on `PORT: 9000`
- Client app will be running on `http://localhost:3000`
- For running the unit tests (client), enter the command: `npm run test-client`

### Contact Info

**_Mauricio Mejia <mauriciomejia09@gmail.com>_**
