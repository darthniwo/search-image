var express = require("express");
var router = express.Router();
var gis = require("g-i-s");

router.get("/:query", (req, res, next) => {
  const query = req.params.query;
  console.log("QUERYSTRING: ", query);
  gis(query, (error, results) => {
    if (error) {
      console.log(error);
    } else {
      res.send(Object.values(results));
    }
  });
});

module.exports = router;
