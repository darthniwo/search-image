import { useState } from "react";
import Search from "./components/Search";
import ImageList from "./components/ImageList";
import Loader from "./components/Loader";
import "./carousel.css";
import "./App.css";
import carousel from "./carousel";

function App() {
  const [imageArray, setImageArray] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const doSearch = async (keyString) => {
    setLoading(true);
    setImageArray([]);
    const res = await fetch(`http://localhost:9000/search/${keyString}`);
    const data = await res.json();
    setImageArray(Array.from(data));
    setLoading(false);
    carousel.initCarousel();
  };

  return (
    <main className="main">
      <h1 data-testid="main-title">
        <span>Image</span>Search
      </h1>
      <Search onSearch={doSearch} isLoading={isLoading} />
      {isLoading ? <Loader /> : null}
      {imageArray.length === 0 ? null : <ImageList imagesArray={imageArray} />}
    </main>
  );
}

export default App;
