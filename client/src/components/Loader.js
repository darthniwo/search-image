function Loader() {
  return (
    <div className="loading" data-testid="loader">
      <div></div>
      <div></div>
    </div>
  );
}

export default Loader;
