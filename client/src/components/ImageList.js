import PropTypes from "prop-types";
import { useState } from "react";
import Modal from "./Modal";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";

function ImageList({ imagesArray }) {
  const [toggleModal, setToggleModal] = useState(false);
  const [activeImage, setActiveImage] = useState({});

  const onBrokenURL = (e) => {
    e.target.onerror = null;
    e.target.src =
      "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/1200px-No-Image-Placeholder.svg.png";
  };

  const imageClicked = (e) => {
    const url = e.target.src;
    setActiveImage(url);
    setToggleModal(true);
  };

  const onCloseModal = () => {
    setToggleModal(false);
  };

  const renderImage = (img, i) => {
    if (!img.url) {
      return null;
    }
    return (
      <img
        data-testid={`resource-img-${i}`}
        key={`resource-img-${i}`}
        className={`carousel__photo ${i === 0 ? "initial" : ""}`}
        onClick={(e) => imageClicked(e)}
        src={`${img.url}`}
        alt={`resource-${i}`}
        onError={onBrokenURL}
      />
    );
  };

  return (
    <div data-testid="carousel-container">
      <div className="carousel-wrapper">
        <div className="carousel" data-testid="carousel">
          {imagesArray.length === 0 ? (
            <span data-testid="empty">No Images</span>
          ) : (
            imagesArray.map((img, i) => {
              return renderImage(img, i);
            })
          )}
          <div className="carousel__button--next" data-testid="next-image">
            <FaAngleRight style={{ fontSize: "30px", color: "#FFF" }} />
          </div>
          <div className="carousel__button--prev" data-testid="prev-image">
            <FaAngleLeft style={{ fontSize: "30px", color: "#FFF" }} />
          </div>
        </div>
      </div>
      {toggleModal ? (
        <Modal
          url={activeImage}
          onCloseModal={onCloseModal}
          onBrokenURL={onBrokenURL}
        />
      ) : null}
    </div>
  );
}
ImageList.defaultProps = {
  imagesArray: [],
};

ImageList.propTypes = {
  imagesArray: PropTypes.array,
};
export default ImageList;
