import PropTypes from "prop-types";
import { FaTimesCircle } from "react-icons/fa";

const Modal = ({ url, onBrokenURL, onCloseModal }) => {
  return (
    <div className="modal-overlay" data-testid="modal-component">
      <div className="modal-container">
        <FaTimesCircle
          data-testid="modal-close"
          className="close"
          onClick={onCloseModal}
          style={{ cursor: "pointer", color: "#FFF" }}
        />
        {url === "" ? null : (
          <img
            className="modal-image"
            src={`${url}`}
            alt="modal-resource"
            onError={onBrokenURL}
          />
        )}
      </div>
    </div>
  );
};

Modal.defaultProps = {
  url: "",
};

Modal.propTypes = {
  url: PropTypes.string,
  onBrokenURL: PropTypes.func,
  onCloseModal: PropTypes.func,
};

export default Modal;
