import PropTypes from "prop-types";
import { FaSistrix } from "react-icons/fa";
import { useState } from "react";

const Search = ({ onSearch, isLoading }) => {
  const [searchKey, setSearchkey] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    if (!searchKey || searchKey === "") {
      return;
    }
    onSearch(searchKey);
  };
  return (
    <div data-testid="search-component">
      <form onSubmit={onSubmit}>
        <div className="search-box">
          <FaSistrix style={{ color: "#42b883" }} />
          <input
            data-testid="search-input"
            type="text"
            placeholder="search"
            value={searchKey}
            disabled={isLoading}
            onChange={(e) => setSearchkey(e.target.value)}
          />
        </div>
        <input
          data-testid="search-submit"
          type="submit"
          className="btn btn--green"
          value="Search"
        />
      </form>
    </div>
  );
};

Search.defaultProps = {};

Search.propTypes = {
  onSearch: PropTypes.func,
  isLoading: PropTypes.bool,
};

export default Search;
