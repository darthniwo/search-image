import { render, fireEvent } from "@testing-library/react";
import Search from "../components/Search";

describe("Search Component", () => {
  test("Should render the component", () => {
    const { queryByTestId } = render(<Search />);
    const main = queryByTestId("search-component");
    expect(main).toBeVisible();
  });

  test("Should change the input's value", () => {
    const { queryByTestId } = render(<Search />);
    const searchInput = queryByTestId("search-input");
    fireEvent.change(searchInput, { target: { value: "Gundam" } });
    expect(searchInput.value).toBe("Gundam");
  });

  test("Should not call onSearch method when search value is empty", () => {
    const onSearch = jest.fn();
    const { queryByTestId } = render(<Search onSearch={onSearch} />);
    const submitInput = queryByTestId("search-submit");
    fireEvent.click(submitInput);
    expect(onSearch).not.toHaveBeenCalled();
  });

  test("Should call onSearch method when reaching onSubmit", () => {
    const onSearch = jest.fn();
    const { queryByTestId } = render(<Search onSearch={onSearch} />);
    const searchInput = queryByTestId("search-input");
    fireEvent.change(searchInput, { target: { value: "Gundam" } });
    const submitInput = queryByTestId("search-submit");
    fireEvent.click(submitInput);
    expect(onSearch).toHaveBeenCalled();
  });
});
