// import { render, screen } from '@testing-library/react';
// import App from './App';

// test('renders learn react link', () => {
//   render(<App />);
//   const linkElement = screen.getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

import { render, fireEvent, waitFor } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import App from "../App";

const imagesArray = [
  {
    url: "https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Gundam.jpg/250px-Gundam.jpg",
    width: 250,
    height: 188,
  },
  {
    url: "https://upload.wikimedia.org/wikipedia/en/d/d6/RX-78-2_Gundam_illustration.gif",
    width: 264,
    height: 377,
  },
  {
    url: "https://www.animenewsnetwork.com/thumbnails/crop1200x630gH0/herald/23265/mobile.suit.gundam.00.full.189661.jpg",
    width: 1200,
    height: 630,
  },
  {
    url: "https://cdn.vox-cdn.com/thumbor/qfa2540z-OnDKMg6ywi7lRBXJRE\\u003d/0x0:1024x683/1200x800/filters:focal(0x0:1024x683)/cdn.vox-cdn.com/uploads/chorus_image/image/41776422/3623630477_4c63af54f9_b.0.0.jpg",
    width: 1200,
    height: 800,
  },
  {
    url: "https://www.animenewsnetwork.com/thumbnails/crop1200x630gH0/herald/23265/mobile.suit.gundam.00.full.189661.jpg",
    width: 1200,
    height: 630,
  },
];

describe("Main container tests", () => {
  const server = setupServer(
    rest.get("http://localhost:9000/search/:query", (req, res, ctx) => {
      return res(ctx.json(imagesArray));
    })
  );

  beforeAll(() => server.listen());
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  test("Should render the main content", () => {
    const { queryByTestId } = render(<App />);
    const mainTitle = queryByTestId("main-title");
    expect(mainTitle).toBeVisible();
  });

  test("Should call the API", async () => {
    const { queryByTestId } = render(<App />);
    const searchInput = queryByTestId("search-input");
    fireEvent.change(searchInput, { target: { value: "Gundam" } });
    const submitInput = queryByTestId("search-submit");
    fireEvent.click(submitInput);
    const loader = queryByTestId("loader");
    expect(loader).toBeVisible();
    await waitFor(() => {
      const image0 = queryByTestId("resource-img-0");
      expect(image0).toBeVisible();
    });
  });
});
