import { render } from "@testing-library/react";
import Modal from "../components/Modal";

describe("Modal component", () => {
  test("Should render the component", () => {
    const { queryByTestId } = render(<Modal />);
    const main = queryByTestId("modal-component");
    expect(main).toBeVisible();
  });
});
