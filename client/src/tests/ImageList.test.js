import { render, fireEvent, waitFor } from "@testing-library/react";
import ImageList from "../components/ImageList";
import carousel from "../carousel";

const imagesArray = [
  {
    url: "https://upload.wikimedia.org/wikipedia/en/thumb/9/99/Gundam.jpg/250px-Gundam.jpg",
    width: 250,
    height: 188,
  },
  {
    url: "https://upload.wikimedia.org/wikipedia/en/d/d6/RX-78-2_Gundam_illustration.gif",
    width: 264,
    height: 377,
  },
  {
    url: "https://www.animenewsnetwork.com/thumbnails/crop1200x630gH0/herald/23265/mobile.suit.gundam.00.full.189661.jpg",
    width: 1200,
    height: 630,
  },
  {
    url: "https://cdn.vox-cdn.com/thumbor/qfa2540z-OnDKMg6ywi7lRBXJRE\\u003d/0x0:1024x683/1200x800/filters:focal(0x0:1024x683)/cdn.vox-cdn.com/uploads/chorus_image/image/41776422/3623630477_4c63af54f9_b.0.0.jpg",
    width: 1200,
    height: 800,
  },
  {
    url: "https://www.animenewsnetwork.com/thumbnails/crop1200x630gH0/herald/23265/mobile.suit.gundam.00.full.189661.jpg",
    width: 1200,
    height: 630,
  },
];

const fallbackSrc =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/1200px-No-Image-Placeholder.svg.png";

describe("ImageList component", () => {
  test("Should render the component", () => {
    const { queryByTestId } = render(<ImageList imagesArray={[]} />);
    const wrapper = queryByTestId("carousel-container");
    expect(wrapper).toBeVisible();
  });

  test("Should show empty state", () => {
    const { queryByTestId } = render(<ImageList imagesArray={[]} />);
    const wrapper = queryByTestId("empty");
    expect(wrapper).toBeVisible();
  });

  test("Should render images", () => {
    const { queryByTestId } = render(<ImageList imagesArray={imagesArray} />);
    const emptyState = queryByTestId("empty");
    expect(emptyState).not.toBeTruthy();
    const image0 = queryByTestId("resource-img-0");
    expect(image0).toBeVisible();
  });

  test("Should have an initial image", () => {
    const { queryByTestId } = render(<ImageList imagesArray={imagesArray} />);
    const image0 = queryByTestId("resource-img-0");
    expect(image0).toHaveClass("initial");
  });

  test("Should render fallback image", () => {
    const data = [...imagesArray];
    data[0].url = "---";
    const { queryByTestId } = render(<ImageList imagesArray={data} />);
    const image0 = queryByTestId("resource-img-0");
    fireEvent.error(image0);
    expect(image0).toHaveAttribute("src", fallbackSrc);
  });

  test("Should expand the clicked image", () => {
    const { queryByTestId } = render(<ImageList imagesArray={imagesArray} />);
    const image0 = queryByTestId("resource-img-0");
    fireEvent.click(image0);
    const modal = queryByTestId("modal-component");
    expect(modal).toBeVisible();
    const modalCloseBtn = queryByTestId("modal-close");
    fireEvent.click(modalCloseBtn);
    expect(modal).not.toBeVisible();
  });

  test("Should move to next image", async () => {
    const { queryByTestId } = render(<ImageList imagesArray={imagesArray} />);
    carousel.initCarousel();
    const image0 = queryByTestId("resource-img-0");
    const image1 = queryByTestId("resource-img-1");
    expect(image0).toHaveClass("active");
    expect(image1).toHaveClass("next");
    const nextBtn = queryByTestId("next-image");
    fireEvent.click(nextBtn);
    expect(image0).toHaveClass("prev");
    expect(image1).toHaveClass("active");
    const prevBtn = queryByTestId("prev-image");
    await waitFor(() => {
      fireEvent.click(prevBtn);
      expect(image1).not.toHaveClass("active");
    });
  });
});
