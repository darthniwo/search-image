import { render } from "@testing-library/react";
import Loader from "../components/Loader";

describe("Loader component", () => {
  test("Should render the loader", () => {
    const { queryByTestId } = render(<Loader />);
    const loader = queryByTestId("loader");
    expect(loader).toBeTruthy();
  });
});
